﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
//using System.Threading;
using System.Windows.Forms;
using System.Drawing;

using System.IO.Compression;
using System.IO;
using System.Diagnostics;
using WSAInstallTool.Util;
using ImageMagick; // 确保添加了 using 指令




namespace WSAInstallTool
{
    class AAPTParseUtil
    {
        private string appInfo = "";
        private string[] appInfoList = null;

        public Boolean ConvertWebPtoPNG(string webpFilePath, string pngFilePath)
        {
            try {
                // 读取WebP图片
                using (var image = new MagickImage(webpFilePath))
                {
                    // 将WebP图片转换为PNG格式
                    image.Format = MagickFormat.Png;

                    // 保存转换后的PNG图片
                    image.Write(pngFilePath);
                }
                return true;

            }
            catch (Exception e)
            {
                Debug.WriteLine("Convert webp to png error! " + e.Message);
                return false;
            }

        }

        public AAPTParseUtil(string appInfo)
        {
            if (appInfo == null || appInfo.Length == 0)
            {
                return;
            }
            this.appInfo = appInfo;
            this.appInfoList = appInfo.Split('\n');
            //Console.WriteLine(appInfoList.Length);
        }

        public string GetPackageName()
        {
            if (appInfo.Length == 0 || appInfoList.Length < 1) return "";

            return GetValue(appInfoList[0], "package: name='", "' versionCode='");
        }

        /// <summary>
        /// 获取版本名称
        /// </summary>
        /// <returns></returns>
        public string GetVersionName()
        {
            if (appInfo.Length == 0 || appInfoList.Length < 1) return "";

            if (appInfoList[0].Contains("' platformBuildVersionName="))
            {
                return GetValue(appInfoList[0], "versionName='", "' platformBuildVersionName=");
            }

            return GetValue(appInfoList[0], "versionName='", "' compileSdkVersion='");
        }

        /// <summary>
        /// 获取版本code
        /// </summary>
        /// <returns></returns>
        public string GetVersionCode()
        {
            if (appInfo.Length == 0 || appInfoList.Length < 1) return "";

            if (appInfoList[0].Contains("' platformBuildVersionName="))
            {
                return GetValue(appInfoList[0], "versionCode='", "' versionName='");
            }

            return GetValue(appInfoList[0], "version_code='", "' version_name='");
        }

        /// <summary>
        /// 获取详细的权限列表
        /// </summary>
        /// <returns></returns>
        public string GetPermissionDetail()
        {
            if (appInfo.Length == 0 || appInfoList.Length < 1) return "";
            StringBuilder sb = new StringBuilder();
            foreach (string str in appInfoList)
            {
                if (str.Contains("uses-permission:"))
                {
                    sb.Append(str).Append("\n");
                }
            }
            return sb.ToString().Trim();
        }

        /// <summary>
        /// 获取详细的权限信息解释 返回String
        /// </summary>
        /// <returns></returns>
        public string GetPermissionDetailString()
        {
            if (appInfo.Length == 0 || appInfoList.Length < 1) return "";

            StringBuilder sb = new StringBuilder();

            List<String> temp = GetPermissionList();

            foreach (String str in temp)
            {

                //if (str.Contains(AndroidPerimission.READ_EXTERNAL_STORAGE.permissionName))
                //{
                //    sb.Append(AndroidPerimission.READ_EXTERNAL_STORAGE.description).Append("\n");
                //}
                //Console.WriteLine("....." + AndroidPerimission.Values + "..." + str);
                foreach (AndroidPerimission permission in AndroidPerimission.Values)
                {
                    //Console.WriteLine("....." + permission.description);
                    //Console.WriteLine("....." + permission.permission);
                    if (str == permission.permission)
                    {
                        sb.Append("· ").Append(permission.description).Append("\n");
                    }
                }
            }

            return sb.ToString().Trim();
        }

        /// <summary>
        /// 获取详细的权限信息解释 返回List
        /// </summary>
        /// <returns></returns>
        public List<string> GetPermissionDetailList()
        {
            if (appInfo.Length == 0 || appInfoList.Length < 1) return new List<string>();

            List<string> result = new List<string>();

            List<String> temp = GetPermissionList();

            foreach (String str in temp)
            {
                foreach (AndroidPerimission permission in AndroidPerimission.Values)
                {
                    if (str == permission.permission)
                    {
                        result.Add(permission.description);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 获取权限信息列表 返回List
        /// </summary>
        /// <returns>List<string></returns>
        public List<string> GetPermissionList()
        {
            if (appInfo.Length == 0 || appInfoList.Length < 1) return new List<string>();

            StringBuilder sb = new StringBuilder();

            List<String> temp = new List<String>();

            foreach (string str in appInfoList)
            {
                if (str.Contains("uses-permission: name='"))
                {
                    if (!temp.Contains(str))
                    {
                        temp.Add(GetValue(str, "uses-permission: name='", "'"));
                    }
                }
            }
            return temp;
        }

        /// <summary>
        /// 获取APK图标
        /// </summary>
        /// <param name="apkPath"></param>
        /// <returns></returns>
        public string GetApkIcon(string apkPath)
        {
            string result = System.Threading.Thread.GetDomain().BaseDirectory + "icon\\" + GetPackageName() + ".png";
            List<string> logoFileOtherExtensionList = new List<string> { ".xml", ".webp" };
            List<string> logoFileNamelist = new List<string> { };
            string logoFileExtension = "";
            Boolean isWebp = false;

            if (File.Exists(result))
            {
                //Console.WriteLine("logo exist！");

                FileInfo fileInfo = new FileInfo(result);
                if (fileInfo.Length < 1024) // 文件大小小于1KB
                {
                    // 防止 webp 转 png 失败的文件不更新 png
                    fileInfo.Delete(); // 删除文件
                }
                else {
                    return result;
                }
            }            
            try
            {
                //ZipFile.ExtractToDirectory(apkPath, "temp"); //解压
                string logoPath = "";
                // 1. 获取logo
                foreach (string str in appInfoList)
                {
                    if (str.Contains("application-icon-160"))
                    {
                        logoPath = GetValue(str.Trim(), "application-icon-160:'", "'");
                        break;
                    }

                    if (str.Contains("application-icon-120"))
                    {
                        logoPath = GetValue(str.Trim(), "application-icon-120:'", "'");
                        break;
                    }
                }
                //MessageBox.Show(logoPath, "提示");
                if (logoPath == null || logoPath.Length == 0) return "";
                string logoFileName = Path.GetFileName(logoPath);
                logoFileNamelist.Add(logoFileName);
                // 获取 apkInfo 里面的 icon 的扩展名
                string logoPathExtension = Path.GetExtension(logoPath).ToLower();
                Debug.WriteLine("[AAPTParseUtil][GetApkIcon]logoFileName==" + logoFileName);
                // 解决 logo 路径为 'res/mipmap-anydpi-v26/ic_launcher.xml，logo 格式为 webp 不显示的问题
                // 还有一种情况是 .xml 的路径，logo 格式是 .png 的
                if (logoFileOtherExtensionList.Contains(logoPathExtension))
                {
                    logoFileNamelist.Add(logoFileName.Replace(".xml",".webp"));
                    logoFileNamelist.Add(logoFileName.Replace(".webp", ".png"));
                }

                // 2. 解压logo
                using (ZipArchive zipArchive = ZipFile.Open(apkPath, ZipArchiveMode.Read))
                {
                    // 遍历zip文件中的所有条目
                    foreach (ZipArchiveEntry entry in zipArchive.Entries)
                    {

                        // .webp .png 对应的 logo 都找
                        if (logoFileNamelist.Contains(entry.Name))
                        {   
                            //Console.WriteLine(entry.Name);
                            // 获取条目的扩展名
                            logoFileExtension = Path.GetExtension(entry.FullName);
                            if (logoFileExtension == ".webp")
                            {
                                isWebp = true;
                                result = System.Threading.Thread.GetDomain().BaseDirectory + "icon\\" + GetPackageName() + ".webp";
                            }
                            
                            using (Stream stream = entry.Open())
                            {
                                // 创建 icon 缓存文件夹
                                if (!Directory.Exists(System.Threading.Thread.GetDomain().BaseDirectory + "icon"))
                                {
                                    Directory.CreateDirectory(System.Threading.Thread.GetDomain().BaseDirectory + "icon");
                                }
                                // 将找到的文件解压到 result 路径所指的位置，如果 result 指的文件已经存在则覆盖它（true 参数表明允许覆盖）                                                   
                                entry.ExtractToFile(result, true);                               
                                if (isWebp)
                                {
                                    // 先保存 webp 再把 webp 转成 png(System.Windows.Forms.PictureBox 控件本身并不直接支持 webp 格式的图片)
                                    if (ConvertWebPtoPNG(result, result.Replace(".webp", ".png"))) {
                                        // webp 转完 png 后（有可能会转失败），再返回 png 的路径
                                        result = System.Threading.Thread.GetDomain().BaseDirectory + "icon\\" + GetPackageName() + ".png";
                                    }
                                }

                                using (Image image = Image.FromFile(result))
                                if (image.Width >= 100) {
                                    // 找到分辨率合适的就返回
                                    return result;
                                }
                            }
                        }
                    }
                }
                Debug.WriteLine("apk logo path => " + result);
                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine("get apk icon error! " + e.Message);
                return "";
            }
        }

        /// <summary>
        /// 获取APP名称
        /// </summary>
        /// <returns></returns>
        public string GetAppName()
        {
            if (appInfo.Length == 0 || appInfoList.Length < 1) return "";
            string zhName = "";
            string defaultName = "";
            foreach (string str in appInfoList)
            {
                if (str.Contains("application-label-zh:"))
                {

                    //return GetValue(str, "application-label:", "");
                    zhName = GetValue(str, "application-label-zh:'", "'");
                }
                else if (str.Contains("application-label:"))
                {
                    defaultName = GetValue(str, "application-label:'", "'");
                }
            }

            if (zhName != null && zhName != "" && zhName.Length != 0)
            {
                return Utf82Gb2312(zhName);
            }
            else if (defaultName != null && defaultName != "" && defaultName.Length != 0)
            {
                return defaultName;
            }
            return "";
        }

        public int GetMinSdkVersion()
        {
            if (appInfo.Length == 0 || appInfoList.Length < 1)
            {
                return -1;
            }

            foreach (string s in appInfoList)
            {
                if (s.Contains("sdkVersion:'"))
                {
                    try
                    {
                        var result = int.Parse(GetValue(s, "sdkVersion:'", "'"));
                        return result;
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("GetMinSdkVersion Error! = " + e.Message);
                        return -1;
                    }
                }
            }

            return -1;
        }

        /// <summary>
        /// 获取最小支持的android版本
        /// </summary>
        /// <returns></returns>
        public string getMinSupportVersion()
        {
            int minSdk = GetMinSdkVersion();
            if (minSdk == -1)
            {
                return LangUtil.Instance.GetUnrecognizedVersion();
            }
            foreach (AndroidSDKVersion sdkVersion in AndroidSDKVersion.Values)
            {
                if (sdkVersion.api == minSdk)
                {
                    return "Android " + sdkVersion.version;
                }
            }
            return LangUtil.Instance.GetUnrecognizedVersion();
        }

        /// <summary>
        /// 获得字符串中开始和结束字符串中间得值
        /// </summary>
        /// <param name="str">字符串</param>
        /// <param name="s">开始</param>
        /// <param name="e">结束</param>
        /// <returns></returns>
        public string GetValue(string str, string s, string e)
        {   
            string value;
            Regex rg = new Regex("(?<=(" + s + "))[.\\s\\S]*?(?=(" + e + "))", RegexOptions.Multiline | RegexOptions.Singleline);
            value = rg.Match(str).Value;
            if (String.IsNullOrWhiteSpace(value))
            {
                return "";
            }
            else { 
                return value;
            }
   
        }

        /// <summary>
        /// UTF8转换成GB2312
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public string Utf82Gb2312(string text)
        {
            //声明字符集   
            System.Text.Encoding utf8, gb2312;
            //utf8   
            utf8 = System.Text.Encoding.GetEncoding("utf-8");
            //gb2312   
            gb2312 = System.Text.Encoding.GetEncoding("gb2312");
            byte[] utf;
            utf = utf8.GetBytes(text);
            utf = System.Text.Encoding.Convert(utf8, gb2312, utf);
            //返回转换后的字符   
            return gb2312.GetString(utf);
        }
    }
}
